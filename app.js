const appRoot = document.getElementById('app-root');

let currentCountries = externalService.getAllCountries();
let currentSort = undefined;

const table = document.createElement('table');
const searchBlock = document.createElement('div');

function renderSearchElments () {
    searchBlock.append(renderInput('Поиcк по региону'));
    searchBlock.append(renderInput('Поиcк по языку', 'lang'));
    appRoot.append(searchBlock);
}

function renderInput (text, searchType='region') {
    const label = document.createElement('label');
    label.textContent = text;
    const input = document.createElement('input');
    input.type = 'radio';
    input.name = 'search';
    input.addEventListener('click', () => {
        //* Не совсем понял, надо ли делать сброс при выборе другого radio button
        const select = document.getElementById('select');
        if (select && select.value != 'Не выбрано') {
            currentCountries = externalService.getAllCountries();
            resetSortButtons();
            renderTableBody();
            currentSort = undefined;
        }
        //* 

        const resetButton = document.getElementById('reset-button');
        if (resetButton) resetButton.remove();
        renderSelect(searchType);
    });
    label.append(input);
    return label;
}

function renderSelect (searchType) {
    const oldSelect = document.getElementById('select');
    if (oldSelect) oldSelect.remove();
    const select = document.createElement('select');
    select.id = 'select';
    select.innerHTML = '<option disabled selected hidden>Не выбрано</option>';
    let values;
    searchType == 'region' ? values = externalService.getRegionsList() : values = externalService.getLanguagesList();
    values.forEach(e => {
        select.innerHTML += `<option value='${e}'>${e}</option>`;
    })
    select.addEventListener('change', (e) => {
        getCountryListBy(searchType, e.target.value);
        renderResetButton();
    });
    searchBlock.append(select);
}

function renderResetButton () {
    const oldResetButton = document.getElementById('reset-button');
    if (oldResetButton) oldResetButton.remove();
    const button = document.createElement('button');
    button.id = 'reset-button';
    button.textContent = 'Сбросить';
    button.addEventListener('click', e => {
        e.currentTarget.remove();
        document.getElementById('select').remove();
        document.querySelector('input[name="search"]:checked').checked = false;
        currentCountries = externalService.getAllCountries();
        resetSortButtons();
        renderTableBody();
        currentSort = undefined;
    })
    searchBlock.append(button);
}

function getCountryListBy (searchType, value) {
    if (searchType == 'region') currentCountries = externalService.getCountryListByRegion(value);
    else if (searchType == 'lang') currentCountries = externalService.getCountryListByLanguage(value);

    resetSortButtons();
    renderTableBody();
    currentSort = undefined;
}

function renderTableHead() {
    const header = table.createTHead();
    const row = header.insertRow();
    const columnNames = ['Название страны','Столица', 'Регион', 'Языки', 'Площадь', 'Флаг'];
    columnNames.forEach((value, index) => {
        const cell = document.createElement('th');
        cell.textContent = value;
        if (index == 0) cell.append(renderSortButton());
        if (index == 4) cell.append(renderSortButton('numbers'));
        row.append(cell);
    })
    appRoot.append(table);
}

function renderSortButton (by='alphabet') {
    const div = document.createElement('div');
    div.innerHTML = `&UpArrow;`;
    div.classList.add('sort-button');
    div.addEventListener('click', (e) => {
        if (e.target.classList.contains('active')) {
            e.target.classList.contains('rotate') ? e.target.classList.remove('rotate') : e.target.classList.add('rotate');
        } else {
            resetSortButtons();
            e.target.classList.add('active');
        }
        sort(by);
    })
    return div;
}

function resetSortButtons() {
    document.getElementsByClassName('sort-button')[0].classList.remove('active','rotate');
    document.getElementsByClassName('sort-button')[1].classList.remove('active','rotate');
}

function sort (by='alphabet') {
    if (by == 'alphabet') {
        if (currentSort != 'alphabet') {
            currentCountries.sort((a, b) => (a.name).localeCompare(b.name));
            currentSort = 'alphabet';
        } else {
            currentCountries.reverse();
        }
    }
    else if (by == 'numbers') {
        if (currentSort != 'numbers') {
            currentCountries.sort((a, b) => Number(a.area) - Number(b.area));
            currentSort = 'numbers';
        } else {
            currentCountries.reverse();
        }
    } 
    renderTableBody();
}

function renderTableBody() {
    const oldTbody = document.getElementsByTagName('tbody')[0];
    if (oldTbody) oldTbody.remove();
    const tbody = table.createTBody();
    currentCountries.forEach(e => {
        const row = tbody.insertRow();

        row.insertCell().textContent = e.name;
        row.insertCell().textContent = e.capital;
        row.insertCell().textContent = e.region;
        row.insertCell().textContent = Object.values(e.languages).join(', ');
        row.insertCell().textContent = e.area;

        const flag = row.insertCell();
        flag.style.backgroundImage = "url('"+e.flagURL+"')";
        flag.classList.add('flag-cell');
    })
}

renderSearchElments();
renderTableHead();
renderTableBody();